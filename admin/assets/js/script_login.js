$(document).ready(function () {
    $('#Form-login').on('submit', function (e) {
        e.preventDefault();

        data = $(this).serializeArray();

        data.push({
            'name': 'login[opc]',
            'value': 'Nuevo-inicio'
        });

        $.post('assets/libs/logueo', data, function (data) {
            if (data.status == true) {
                $('#Form-login')[0].reset();

                // notification('success', 'Ingresando...');
                window.location.href = data.redirect;
            } else {
                // notification('error', 'Usuario o contraseña incorrectos');
            }
        }, 'json');
    })
});
