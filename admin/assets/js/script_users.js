$(document).ready(function () {
    data = new FormData();
    var userId = 0;

    listar_usuarios(1);

    $('.Add-user').on('click', function () {
        userId = 0;
        $('#Form-user').find('[name="user[password]"]').attr('required', true);
        $('.temp-destacada').remove();
    })

    var $uploadCrop,
        tempFilename,
        rawImg;

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                rawImg = e.target.result;
            };
            reader.readAsDataURL(input.files[0]);
        } else {
            nmensaje('error', 'Error', 'Su navegador no soporta FileReader API');
        }
    }

    $('.img-destacada').on('click', function () {
        $('#destacada').click();
    });

    $('#destacada').on('change', function () {
        if ($(this).val() !== '') {
            if (/^image\/\w+/.test(this.files[0].type)) {
                tempFilename = $(this).val().replace(/C:\\fakepath\\/i, '');
                tempFilename = tempFilename.substr(0, tempFilename.lastIndexOf('.')) || tempFilename;
                readFile(this);
                modal({
                    type: 'info',
                    title: 'Crop Image',
                    text: '<div class="modal-body">' +
                        '<div id="upload-demo" class="center-block"></div>' +
                        '</div>',
                    size: 'large',
                    callback: function (result) {
                        if (result === true) {
                            $uploadCrop.croppie('result', {
                                type: 'base64',
                                format: 'jpeg',
                                size: 'original',
                                // size: {
                                // 	width: 800,
                                // 	height: 500
                                // },
                            }).then(function (resp) {
                                data.append('user[img-usuario]', resp);
                                data.append('user[img-usuario-nombre]', tempFilename);
                                $('.temp-destacada').remove();
                                $('#destacada').after('<div class="Pictures-des-mos temp-destacada"><br>' +
                                    ' <div class="Pictures-des-mos-int">' +
                                    '<img src="' + resp + '">' +
                                    '</div>' +
                                    '<div class="Pictures-des-mos-name-image">' +
                                    '<p>' + tempFilename + '</p>' +
                                    '</div>' +
                                    '<div class="Pictures-des-mos-edit Left">' +
                                    '<a href="javascript://" class="img-eliminar">Delete</a>' +
                                    '</div>' +
                                    '</div>');
                            });
                        }
                    },
                    onShow: function (result) {
                        $uploadCrop = $('#upload-demo').croppie({
                            viewport: {
                                width: 180,
                                height: 180,
                            },
                            enforceBoundary: false,
                            enableExif: true,
                            enableResize: true
                        });
                        $uploadCrop.croppie('bind', {
                            url: rawImg
                        }).then(function () {
                            console.log('jQuery bind complete');
                        });
                    },
                    closeClick: false,
                    animate: true,
                    buttonText: {
                        yes: 'Confirmar',
                        cancel: 'Cancelar'
                    }
                });
            } else {
                // nmensaje('error', 'Error', 'No es un formato válido');
                $(this).val('');
            }
        }
    });

    $('body').on('click', '.img-eliminar', function () {
        data.append('eliminar-imagen', 1);
        $('.temp-destacada').remove();
    });

    $('#Form-user').on('submit', function (e) {
        e.preventDefault();
        $('[type=submit]', this).prop('disabled', true);

        if (!this.checkValidity()) {
            e.stopPropagation()
            this.classList.add('was-validated')
        } else {
            $('.load-form').show();
            otradata = $(this).serializeArray();

            $.each(otradata, function (key, input) {
                data.append(input.name, input.value);
            });

            data.append('user[opc]', 'nuevo-usuario');
            data.append('user[id]', userId);

            $.ajax({
                url: 'assets/libs/acc_users',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    $('.load-form').hide();
                    $('#destacada').val('');
                    if (data.status == true) {
                        userId = 0;
                        $('#Modal-articulo').modal('hide');
                        $('#Form-user')[0].reset();
                        listar_usuarios(1);
                    } else if (data.status == true && data.error_imagen == true) {
                        // nmensaje('error', 'Error', 'La imagen principal no pudo ser subida');
                    } else if (data.status == false) {
                        // $('.progress-bar', '#progreso_destacada').fadeOut(500, function () {
                        //     $('#progreso_destacada').empty();
                        // });
                        // nmensaje('error', 'Error', data.motivo);
                    }
                }
            });
        }

        $('[type=submit]', this).prop('disabled', false);
    });

    function listar_usuarios(pagina) {
        $('.load-listado-usuarios').show();
        $('#listado-usuarios').empty();
        $.post('assets/libs/acc_users', {
            'user[nombre]': $('.Buscar-nombre').val(),
            'user[fecha-creacion]': $('.Buscar-fecha-creacion').val(),
            'user[estado]': $('.Buscar-estado').val(),
            'user[pagina]': pagina,
            'user[opc]': 'listado-usuarios'
        }, function (data) {
            $('.load-listado-usuarios').hide();
            $('#listado-usuarios').html(data.listado);
            $('.Tabla-usuarios .pagination').html(data.paginacion);
        }, 'json');
    }

    $('body').on('click', '.Tabla-usuarios .mpag', function () {
        listar_usuarios($(this).prop('id'));
    });

    $('.Buscar-usuarios').on('click', function () {
        listar_usuarios(1);
    });

    $('body').on('click', '.Editar-usuario', function () {
        userId = $(this).attr('data-usuario');
        $('.temp-destacada').remove();

        $.post('assets/libs/acc_users', { 'user[id]': userId, 'user[opc]': 'datos-usuario' }, function (data) {
            $('#Form-user')[0].reset();
            $('#Form-user').find('[name="user[name]"]').val(data.datos.nombre);
            $('#Form-user').find('[name="user[lastName]"]').val(data.datos.apellido);
            $('#Form-user').find('[name="user[email]"]').val(data.datos.correo);
            $('#Form-user').find('[name="user[status]"]').val(data.datos.estado);
            $('#Form-user').find('[name="user[password]"]').attr('required', false);
            $('#Modal-articulo').modal('show');
        }, 'json');
    })

});
