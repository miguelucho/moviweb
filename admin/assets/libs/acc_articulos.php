<?php
include("conexion.php");
session_start();

$data = $_REQUEST['articulo'];
$msg = [];

switch ($data['opc']) {
	case 'nuevo-articulo':
		$datos = [
			'titulo_es' => $data['titulo_es'],
			'url_es' => $data['url_es'],
			'meta_es' => $data['meta_descripcion_es'],
			'descripcion_es' => $data['descripcion_es'],
			'estado_es' => $data['estado_es'],
			'titulo_en' => $data['titulo_en'],
			'url_en' => $data['url_en'],
			'meta_en' => $data['meta_descripcion_en'],
			'descripcion_en' => $data['descripcion_en'],
			'estado_en' => $data['estado_en'],
			'autor' => $data['autor']
		];

		if ($data['estado_en'] == 3) {
			$datos['publicado_en'] = date('Y-m-d H:i:s');
		}

		if ($data['estado_es'] == 3) {
			$datos['publicado_es'] = date('Y-m-d H:i:s');
		}

		if ($data['id'] == 0) {
			$datos['creado'] = date('Y-m-d H:i:s');

			$nuevo = $db
				->insert('articulos', $datos);

			if ($nuevo) {
				$data['id'] = $nuevo;
			}
		} else {
			$datos['actualizado'] = date('Y-m-d H:i:s');

			$nuevo = $db
				->where('Id', $data['id'])
				->update('articulos', $datos);
		}

		if ($nuevo) {
			if (isset($data['img-producto'])) {
				$publicacion = $db
					->where('Id', $data['id'])
					->objectBuilder()->get('articulos');

				if ($publicacion[0]->imagen != '' && file_exists('../../../dist/assets/images/' . $publicacion[0]->imagen)) {
					unlink('../../../dist/assets/images/' . $publicacion[0]->imagen);
				}

				$mk         = date('mdYhis');
				$img        = explode(',', $data['img-producto']);
				$img        = base64_decode($img[1]);
				$nombre_img = $data['id'] . '-p_' . $mk . '-' . Limpiar($data['img-producto-nombre']) . '.jpg';
				$archivador = '../../../dist/assets/images/' . $nombre_img;

				if (file_put_contents($archivador, $img)) {
					require 'imagine/vendor/autoload.php';
					$imagine = new Imagine\Gd\Imagine();

					$img = $imagine->open($archivador);
					$img = $img->save($archivador);

					$imgpublica = $db
						->where('Id', $data['id'])
						->update('articulos', ['imagen' => $nombre_img]);

					$msg['error_imagen'] = false;
				} else {
					$msg['error_imagen'] = true;
				}
			} else {
				// if (isset($data['eliminar-imagen'])) {
				// 	$publicacion = $db
				// 		->where('Id', $data['id'])
				// 		->objectBuilder()->get('articulos');

				// 	if ($publicacion[0]->imagen != '' && file_exists('../../../dist/assets/images/' . $publicacion[0]->imagen)) {
				// 		unlink('../../../dist/assets/images/' . $publicacion[0]->imagen);
				// 	}

				// 	$imgpublica = $db
				// 		->where('Id', $data['id'])
				// 		->update('articulos', ['imagen' => '']);
				// }

				$msg['error_imagen'] = 111;
			}

			$msg['status'] = true;
			$msg['mensaje'] = 'Articulo creado correctamente';
		} else {
			$msg['tipo'] = false;
			$msg['mensaje'] = 'Error al crear el articulo';
		}

		echo json_encode($msg);
		break;
	case 'listado-articulos':
		$page       = $data['pagina'];
		$results_pg = 30;
		$adjacent = 2;

		$terms = ['nombre', 'fecha-creacion', 'fecha-publicacion', 'estado'];

		foreach ($terms as $item) {
			if (empty($data[$item])) {
				$data[$item] = '%';
			}
		}

		$totalitems = $db
			->where('titulo_en', '%' . $data['nombre'] . '%', 'LIKE')
			->where('creado', $data['fecha-creacion'] . '%', 'LIKE')
			->where('COALESCE(publicado_en, "")', $data['fecha-publicacion'] . '%', 'LIKE')
			->where('estado_en', $data['estado'], 'LIKE')
			->objectBuilder()->get('articulos');

		$numpags = ceil($db->count / $results_pg);

		if ($numpags >= 1) {
			require_once 'paginacion.php';
			$listado = '';
			$db->pageLimit = $results_pg;

			$listar = $db
				->where('titulo_en', '%' . $data['nombre'] . '%', 'LIKE')
				->where('creado', $data['fecha-creacion'] . '%', 'LIKE')
				->where('COALESCE(publicado_en, "")', $data['fecha-publicacion'] . '%', 'LIKE')
				->where('estado_en', $data['estado'], 'LIKE')
				->orderBy('Id', 'desc')
				->objectBuilder()->paginate('articulos', $page);

			foreach ($listar as $resl) {
				$estado = '';
				$publicado = '';

				switch ($resl->estado_en) {
					case 1:
						$estado = '<span class="Statuslabel">Draft</span>';
						break;
					case 2:
						$estado = '<span class="Statuslabel Statuslabel__check">Check</span>';
						break;
					case 3:
						$estado = '<span class="Statuslabel Statuslabel__publish">Publish</span>';
						$publicado = date('d-m-Y', strtotime($resl->publicado_en));
						break;
				}

				$listado .= '<tr>
								<td class="Align-middle"><span>' . $resl->Id . '</span></td>
								<td class="Align-middle"><span>' . $resl->titulo_en . '</span></td>
								<td class="Align-middle"><span>' . date('d-m-Y', strtotime($resl->creado)) . '</span></td>
								<td class="Align-middle"><span>' . $publicado . '</span></td>
								<td class="Align-middle">' . $estado . '</td>
								<td class="Align-middle text-center"><a href="javascript:void(0);" class="btn btn-outline-primary btn-sm"><i class="bi bi-eye"></i> View</a></td>
								<td class="Align-middle text-center">
									<a href="javascript:void(0);" class="btn btn-outline-primary btn-sm Editar-articulo" data-articulo="' . $resl->Id . '"><i class="bi bi-pencil"></i> Edit</a>
								</td>
							</tr>';
			}

			$msg['listado'] = $listado;
			$pagconfig = array('pagina' => $page, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $results_pg, 'adyacentes' => $adjacent);
			$paginate = new Paginacion($pagconfig);
			$msg['paginacion'] = $paginate->crearlinks();
		} else {
			$msg['listado'] = '<tr>
                                <td colspan="7"><p style="text-align:center">There are no records</p></td>
                            </tr>';
			$msg['paginacion'] = '';
		}

		echo json_encode($msg);
		break;
	case 'datos-articulo':
		$articulo = $db
			->where('Id', $data['id'])
			->objectBuilder()->get('articulos');

		if ($db->count > 0) {
			$users = $db
				->where('Id', $articulo[0]->autor)
				->objectBuilder()->get('usuarios', null, 'Id, CONCAT(nombre," ", apellido) AS nombre');

			if ($db->count > 0) {
				$articulo[0]->autor = $users[0]->nombre;
				$articulo[0]->autor_id = $users[0]->Id;
			} else {
				$articulo[0]->autor = '';
				$articulo[0]->autor_id = '';
			}

			$msg['datos'] = $articulo[0];
		} else {
			$msg['datos'] = [];
		}

		echo json_encode($msg);
		break;
	case 'Buscar-autor':
		$users = $db
			->where('CONCAT(nombre," ", apellido)', '%' . $data['buscar'] . '%', 'LIKE')
			->where('estado', 1)
			->objectBuilder()->get('usuarios', null, 'Id, CONCAT(nombre," ", apellido) AS nombre');

		if ($db->count > 0) {
			$msg = [
				'status' => true,
				'items' => $users
			];
		} else {
			$msg = [
				'status' => false
			];
		}

		echo json_encode($msg);
		break;
}

function Limpiar($String)
{
	$String = mb_strtolower($String);
	$String = str_replace(['á', 'à', 'â', 'ã', 'ª', 'ä'], "a", $String);
	$String = str_replace(['Á', 'À', 'Â', 'Ã', 'Ä'], "A", $String);
	$String = str_replace(['Í', 'Ì', 'Î', 'Ï'], "I", $String);
	$String = str_replace(['í', 'ì', 'î', 'ï'], "i", $String);
	$String = str_replace(['é', 'è', 'ê', 'ë'], "e", $String);
	$String = str_replace(['É', 'È', 'Ê', 'Ë'], "E", $String);
	$String = str_replace(['ó', 'ò', 'ô', 'õ', 'ö', 'º'], "o", $String);
	$String = str_replace(['Ó', 'Ò', 'Ô', 'Õ', 'Ö'], "O", $String);
	$String = str_replace(['ú', 'ù', 'û', 'ü'], "u", $String);
	$String = str_replace(['Ú', 'Ù', 'Û', 'Ü'], "U", $String);
	$String = str_replace(['[', '^', '´', '`', '¨', '~', ']'], "", $String);
	$String = str_replace("ç", "c", $String);
	$String = str_replace("Ç", "C", $String);
	$String = str_replace("ñ", "n", $String);
	$String = str_replace("Ñ", "N", $String);
	$String = str_replace("Ý", "Y", $String);
	$String = str_replace("ý", "y", $String);
	$String = preg_replace('/\s+/', '_', $String);
	$String = str_replace(['(', ')'], '', $String);
	$String = str_replace("-", "_", $String);
	return $String;
}
