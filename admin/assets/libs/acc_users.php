<?php
include("conexion.php");
session_start();

$data = $_REQUEST['user'];
$msg = [];

switch ($data['opc']) {
	case 'nuevo-usuario':
		$datos = [
			'nombre' => $data['name'],
			'apellido' => $data['lastName'],
			'correo' => $data['email'],
			'tipo' => 2,
			'estado' => $data['status'],
		];

		if (isset($data['password']) && trim($data['password']) != '') {
			$datos['contrasena'] = password_hash(trim($data['password']), PASSWORD_DEFAULT);
		}

		if ($data['id'] == 0) {
			$datos['creacion'] = date('Y-m-d H:i:s');

			$nuevo = $db
				->insert('usuarios', $datos);

			if ($nuevo) {
				$data['id'] = $nuevo;
			}
		} else {
			$datos['actualizado'] = date('Y-m-d H:i:s');

			$nuevo = $db
				->where('Id', $data['id'])
				->update('usuarios', $datos);
		}

		if ($nuevo) {
			if (isset($data['img-usuario'])) {
				$usuarios = $db
					->where('Id', $data['id'])
					->objectBuilder()->get('usuarios');

				if ($usuarios[0]->imagen != '' && file_exists('../../../dist/assets/images/' . $usuarios[0]->imagen)) {
					unlink('../../../dist/assets/images/' . $usuarios[0]->imagen);
				}

				$mk         = date('mdYhis');
				$img        = explode(',', $data['img-usuario']);
				$img        = base64_decode($img[1]);
				$nombre_img = $data['id'] . '-user_' . $mk . '-' . Limpiar($data['img-usuario-nombre']) . '.jpg';
				$archivador = '../../../dist/assets/images/' . $nombre_img;

				if (file_put_contents($archivador, $img)) {
					require 'imagine/vendor/autoload.php';
					$imagine = new Imagine\Gd\Imagine();

					$img = $imagine->open($archivador);
					$img = $img->save($archivador);

					$imgpublica = $db
						->where('Id', $data['id'])
						->update('usuarios', ['imagen' => $nombre_img]);

					$msg['error_imagen'] = false;
				} else {
					$msg['error_imagen'] = true;
				}
			} else {
				// if (isset($data['eliminar-imagen'])) {
				// 	$publicacion = $db
				// 		->where('Id', $data['id'])
				// 		->objectBuilder()->get('articulos');

				// 	if ($publicacion[0]->imagen != '' && file_exists('../../../dist/assets/images/' . $publicacion[0]->imagen)) {
				// 		unlink('../../../dist/assets/images/' . $publicacion[0]->imagen);
				// 	}

				// 	$imgpublica = $db
				// 		->where('Id', $data['id'])
				// 		->update('articulos', ['imagen' => '']);
				// }

				$msg['error_imagen'] = 111;
			}

			$msg['status'] = true;
			$msg['mensaje'] = 'Usuario creado correctamente';
		} else {
			$msg['tipo'] = false;
			$msg['mensaje'] = 'Error al crear el usuario';
		}

		echo json_encode($msg);
		break;
	case 'listado-usuarios':
		$page       = $data['pagina'];
		$results_pg = 30;
		$adjacent = 2;

		$terms = ['nombre', 'fecha-creacion'];

		foreach ($terms as $item) {
			if (empty($data[$item])) {
				$data[$item] = '%';
			}
		}

		if($data['estado'] == '') {
			$data['estado'] = '%';
		}

		$totalitems = $db
			->where('CONCAT(nombre," ",apellido)', '%' . $data['nombre'] . '%', 'LIKE')
			->where('COALESCE(creacion,"")', $data['fecha-creacion'] . '%', 'LIKE')
			->where('estado', $data['estado'], 'LIKE')
			->objectBuilder()->get('usuarios');

		$numpags = ceil($db->count / $results_pg);

		if ($numpags >= 1) {
			require_once 'paginacion.php';
			$listado = '';
			$db->pageLimit = $results_pg;

			$listar = $db
				->where('CONCAT(nombre," ",apellido)', '%' . $data['nombre'] . '%', 'LIKE')
				->where('COALESCE(creacion,"")', $data['fecha-creacion'] . '%', 'LIKE')
				->where('estado', $data['estado'], 'LIKE')
				->orderBy('Id', 'desc')
				->objectBuilder()->paginate('usuarios', $page);

			foreach ($listar as $resl) {
				$estado = '';

				switch ($resl->estado) {
					case 1:
						$estado = '<span class="Statuslabel Statuslabel__check">Active</span>';
						break;
					case 0:
						$estado = '<span class="Statuslabel Statuslabel__publish">Inactive</span>';
						break;
				}

				$listado .= '<tr>
								<td class="Align-middle"><span>' . $resl->Id . '</span></td>
								<td class="Align-middle"><span>' . $resl->nombre . ' ' . $resl->apellido . '</span></td>
								<td class="Align-middle"><span>' . date('d-m-Y', strtotime($resl->creacion)) . '</span></td>
								<td class="Align-middle">' . $estado . '</td>
								<td class="Align-middle text-center">
									<a href="javascript:void(0);" class="btn btn-outline-primary btn-sm Editar-usuario" data-usuario="' . $resl->Id . '"><i class="bi bi-pencil"></i> Edit</a>
								</td>
							</tr>';
			}

			$msg['listado'] = $listado;
			$pagconfig = array('pagina' => $page, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpags, 'resultados_pag' => $results_pg, 'adyacentes' => $adjacent);
			$paginate = new Paginacion($pagconfig);
			$msg['paginacion'] = $paginate->crearlinks();
		} else {
			$msg['listado'] = '<tr>
                                <td colspan="6"><p style="text-align:center">There are no records</p></td>
                            </tr>';
			$msg['paginacion'] = '';
		}

		echo json_encode($msg);
		break;
	case 'datos-usuario':
		$usuario = $db
			->where('Id', $data['id'])
			->objectBuilder()->get('usuarios');

		if ($db->count > 0) {
			$msg['datos'] = $usuario[0];
		} else {
			$msg['datos'] = [];
		}

		echo json_encode($msg);
		break;
}

function Limpiar($String)
{
	$String = mb_strtolower($String);
	$String = str_replace(['á', 'à', 'â', 'ã', 'ª', 'ä'], "a", $String);
	$String = str_replace(['Á', 'À', 'Â', 'Ã', 'Ä'], "A", $String);
	$String = str_replace(['Í', 'Ì', 'Î', 'Ï'], "I", $String);
	$String = str_replace(['í', 'ì', 'î', 'ï'], "i", $String);
	$String = str_replace(['é', 'è', 'ê', 'ë'], "e", $String);
	$String = str_replace(['É', 'È', 'Ê', 'Ë'], "E", $String);
	$String = str_replace(['ó', 'ò', 'ô', 'õ', 'ö', 'º'], "o", $String);
	$String = str_replace(['Ó', 'Ò', 'Ô', 'Õ', 'Ö'], "O", $String);
	$String = str_replace(['ú', 'ù', 'û', 'ü'], "u", $String);
	$String = str_replace(['Ú', 'Ù', 'Û', 'Ü'], "U", $String);
	$String = str_replace(['[', '^', '´', '`', '¨', '~', ']'], "", $String);
	$String = str_replace("ç", "c", $String);
	$String = str_replace("Ç", "C", $String);
	$String = str_replace("ñ", "n", $String);
	$String = str_replace("Ñ", "N", $String);
	$String = str_replace("Ý", "Y", $String);
	$String = str_replace("ý", "y", $String);
	$String = preg_replace('/\s+/', '_', $String);
	$String = str_replace(['(', ')'], '', $String);
	$String = str_replace("-", "_", $String);
	return $String;
}
