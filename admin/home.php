<?php
session_start();

if (!isset($_SESSION['User_Movicoms'])) {
  header('Location: login');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/bootstrap-icons/font/bootstrap-icons.min.css">
  <link rel="stylesheet" href="assets/css/stylescssadmin.css?v<?php echo date('YmdHis') ?>">
  <link rel="stylesheet" href="assets/css/jquery.modal.css" />
  <link rel="stylesheet" href="assets/css/croppie.css" />
  <link rel="stylesheet" href="assets/js/jquery-ui-1.13.2/jquery-ui.min.css" />
  <title>Home Movi</title>
  <style type="text/css" media="screen">
    #modal-window .modal-box.modal-size-large {
      width: 100%;
      height: 100%;
    }

    #upload-demo {
      width: 100%;
      height: 85vh;
    }

    .modal-title {
      padding: 5px !important;
    }

    .modal-text {
      padding: 5px !important;
    }
  </style>
</head>

<body>
  <?php
  include_once 'assets/libs/templates/header.php';
  ?>
  <div class="Contenedorglobal">
    <div class="Contenedorglobal__int Tabla-articulos">
      <div class="mb-3">
        <h5>Blog</h5>
        <p>List, create and manage blog articles</p>
      </div>
      <div class="Contensupbtn mb3 d-flex W-full">
        <a href="#!" class="btn btn-primary Add-article" data-bs-toggle="modal" data-bs-target="#Modal-articulo"><i class="bi bi-file-earmark-plus"></i> Create new</a>
      </div>
      <div class="Contentable mt-3">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name of the article</th>
              <th scope="col">Creation date</th>
              <th scope="col">Publication date</th>
              <th scope="col">Status</th>
              <th scope="col" class="text-center" colspan="2">Actions</th>
            </tr>
            <tr>
              <th colspan="2">
                <div class="">
                  <input type="text" placeholder="Enter the name to search" class="form-control Buscar-nombre" name="">
                </div>
              </th>
              <th>
                <div class="">
                  <input type="date" class="form-control Buscar-fecha-creacion" name="">
                </div>
              </th>
              <th>
                <div class="">
                  <input type="date" class="form-control Buscar-fecha-publicacion" name="">
                </div>
              </th>
              <th>
                <div class="">
                  <select class="form-select Buscar-estado" aria-label="Default select example">
                    <option selected="" value=""> Select</option>
                    <option value="1">Draft</option>
                    <option value="2">Check</option>
                    <option value="3">Publish</option>
                  </select>
                </div>
              </th>
              <th class="text-center" colspan="2">
                <button type="button" class="btn btn-outline-primary d-block W-full Buscar-articulos"><i class="bi bi-search"></i> Search</button>
              </th>
            </tr>
          </thead>
          <tbody id="listado-articulos"></tbody>
        </table>
        <!-- Loader de tabla -->
        <div class="text-center">
          <div class="spinner-border text-primary load-listado-articulos" role="status" style="display: none;">
            <span class="visually-hidden">Loading...</span>
          </div>
        </div>
      </div>
      <!-- Paginación -->
      <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-end">
          <!-- <li class="page-item disabled">
            <a class="page-link">Previous</a>
          </li>
          <li class="page-item"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item">
            <a class="page-link" href="#">Next</a>
          </li> -->
        </ul>
      </nav>
    </div>
  </div>

  <div class="modal fade" id="Modal-articulo" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="Modal-articuloLabel" aria-hidden="true">
    <div class="modal-dialog modal-fullscreen modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="Modal-articuloLabel">Manage article</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="mb-3">
            <p>Create, edit or manage your article</p>
          </div>
          <form id="Form-articulo" class="Forms">
            <div class="Contenmanagearticle d-flex W-full">
              <div class="Contenmanagearticle__sec W-middle Articleenglish">
                <div class="Contentform">
                  <div class="mb-3">
                    <label class="form-label">Title:</label>
                    <input type="text" class="form-control titulo-en" name="articulo[titulo_en]" required>
                    <div class="form-text">Enter the title of your article.</div>
                  </div>
                  <div class="mb-3">
                    <label class="form-label">Friendly URL:</label>
                    <input type="text" readonly class="form-control-plaintext url-en" name="articulo[url_en]" value="here-url-friendly">
                    <div class="form-text">Friendly URLs improve user experience and SEO.</div>
                  </div>
                  <div class="mb-3">
                    <label class="form-label">Meta description:</label>
                    <div class="form-floating">
                      <textarea class="form-control" id="articulo-meta-descripcion-en" placeholder="" name="articulo[meta_descripcion_en]"></textarea>
                      <div class="form-text">A good meta description improves the CTR (Click-Through Rate).</div>
                    </div>
                  </div>
                  <div class="mb-3">
                    <label class="form-label">Blog post description:</label>
                    <div class="form-floating">
                      <textarea class="form-control" id="articulo-descripcion-en" placeholder="" name="articulo[descripcion_en]"></textarea>
                    </div>
                  </div>
                  <div class="mb3">
                    <label class="form-label">Status:</label>
                    <select class="form-select" aria-label="Default select example" name="articulo[estado_en]" required>
                      <option selected="" disabled="" value=""> Select</option>
                      <option value="1">Draft</option>
                      <option value="2">Check</option>
                      <option value="3">Publish</option>
                    </select>
                  </div>
                  <br>
                  <p class="msg-size">Imagen size: 5400px X 3600px - 45,72cm X 30,48cm</p>
                  <div class="Panel-galeria-int">
                    <div class="Pictures-des img-destacada">
                      <div class="Pictures-des-btn">
                         <button type="button" class="btn btn-outline-primary d-block W-full Buscar-articulos"><i class="bi bi-search"></i> Click for search the image</button>
                      </div>
                    </div>
                    <div id="progreso_destacada">
                    </div>
                    <input type="file" id="destacada" style="display: none">
                  </div>
                  <br>
                  <div class="mb-3">
                    <label class="form-label">Author:</label>
                    <input type="text" class="form-control autor-articulo" name="" required>
                    <input type="hidden" name="articulo[autor]">
                    <div class="form-text">Enter the author of the article</div>
                  </div>
                </div>
              </div>
              <div class="Contenmanagearticle__sec W-middle Articlespanish">
                <div class="Contentform">
                  <div class="mb-3">
                    <label class="form-label">Titulo:</label>
                    <input type="text" class="form-control titulo-es" name="articulo[titulo_es]" required>
                    <div class="form-text">Ingresa el titulo de tu articulo.</div>
                  </div>
                  <div class="mb-3">
                    <label class="form-label">Url amigable:</label>
                    <input type="text" readonly class="form-control-plaintext url-es" name="articulo[url_es]" value="here-url-friendly">
                    <div class="form-text">Las URLs amigables mejoran la experiencia del usuario y el SEO.</div>
                  </div>
                  <div class="mb-3">
                    <label class="form-label">Meta description:</label>
                    <div class="form-floating">
                      <textarea class="form-control" id="articulo-meta-descripcion-es" placeholder="" name="articulo[meta_descripcion_es]"></textarea>
                      <div class="form-text">Una buena meta description mejora el CTR (Click-Through Rate).</div>
                    </div>
                  </div>
                  <div class="mb-3">
                    <label class="form-label">Descripción del articulo del Blog:</label>
                    <div class="form-floating">
                      <textarea class="form-control" id="articulo-descripcion-es" placeholder="" name="articulo[descripcion_es]"></textarea>
                    </div>
                  </div>
                  <div class="mb3">
                    <label class="form-label">Estado:</label>
                    <select class="form-select" aria-label="Default select example" name="articulo[estado_es]" required>
                      <option selected="" disabled="" value=""> Seleccionar</option>
                      <option value="1">Borrador</option>
                      <option value="2">Revisado</option>
                      <option value="3">Publicado</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer justify-content-start">
          <button type="submit" class="btn btn-primary d-block">
            <div class="spinner-border spinner-border-sm load-form" role="status" style="display: none;">
              <span class="visually-hidden">Loading...</span>
            </div>
            Save information
          </button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <script src="assets/js/jquery-3.7.1.min.js"></script>
  <script src="assets/js/bootstrap.bundle.min.js"></script>
  <script src="assets/js/tinymce/tinymce.min.js"></script>
  <script src="assets/js/croppie.min.js"></script>
  <script src="assets/js/jquery.modal.min.js"></script>
  <script src="assets/js/jquery-ui-1.13.2/jquery-ui.min.js"></script>
  <script src="assets/js/script_home.js?v=2.0"></script>
</body>

</html>
