<?php
session_start();

if (!isset($_SESSION['User_Movicoms'])) {
  header('Location: login');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/bootstrap-icons/font/bootstrap-icons.min.css">
  <link rel="stylesheet" href="assets/css/stylescssadmin.css?v<?php echo date('YmdHis') ?>">
  <link rel="stylesheet" href="assets/css/jquery.modal.css" />
  <link rel="stylesheet" href="assets/css/croppie.css" />
  <title>Users Movi</title>
  <style type="text/css" media="screen">
    #modal-window .modal-box.modal-size-large {
      width: 100%;
      height: 100%;
    }

    #upload-demo {
      width: 100%;
      height: 85vh;
    }

    .modal-title {
      padding: 5px !important;
    }

    .modal-text {
      padding: 5px !important;
    }
  </style>
</head>

<body>
  <?php
  include_once 'assets/libs/templates/header.php';
  ?>
  <div class="Contenedorglobal">
    <div class="Contenedorglobal__int Tabla-articulos">
      <div class="mb-3">
        <h5>Users</h5>
        <p>List, create and manage users</p>
      </div>
      <div class="Contensupbtn mb3 d-flex W-full">
        <a href="#!" class="btn btn-primary Add-user" data-bs-toggle="modal" data-bs-target="#Modal-articulo"><i class="bi bi-file-earmark-plus"></i> Create new</a>
      </div>
      <div class="Contentable mt-3">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">User's Name</th>
              <th scope="col">Creation date</th>
              <th scope="col">Status</th>
              <th scope="col" class="text-center" colspan="2">Actions</th>
            </tr>
            <tr>
              <th colspan="2">
                <div class="">
                  <input type="text" placeholder="Enter the name to search" class="form-control Buscar-nombre" name="">
                </div>
              </th>
              <th>
                <div class="">
                  <input type="date" class="form-control Buscar-fecha-creacion" name="">
                </div>
              </th>
              <th>
                <div class="">
                  <select class="form-select Buscar-estado" aria-label="Default select example">
                    <option selected="" value=""> Select</option>
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                  </select>
                </div>
              </th>
              <th class="text-center" colspan="2">
                <button type="button" class="btn btn-outline-primary d-block W-full Buscar-usuarios"><i class="bi bi-search"></i> Search</button>
              </th>
            </tr>
          </thead>
          <tbody id="listado-usuarios"></tbody>
        </table>
        <!-- Loader de tabla -->
        <div class="text-center">
          <div class="spinner-border text-primary load-listado-usuarios" role="status" style="display: none;">
            <span class="visually-hidden">Loading...</span>
          </div>
        </div>
      </div>
      <!-- Paginación -->
      <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-end">
        </ul>
      </nav>
    </div>
  </div>

  <div class="modal fade" id="Modal-articulo" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="Modal-articuloLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="Modal-articuloLabel">Create User</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="mb-3">
            <p>Create a new user</p>
          </div>
          <form id="Form-user" class="Forms">
            <div class="Contenmanageuser W-full">
              <div class="Contentform">
                <div class="mb-3">
                  <label class="form-label">Name:</label>
                  <input type="text" class="form-control" name="user[name]" required>
                  <div class="form-text">Enter the name of the user.</div>
                </div>
                <div class="mb-3">
                  <label class="form-label">Last name:</label>
                  <input type="text" class="form-control" name="user[lastName]" required>
                  <div class="form-text">Enter the last name of the user.</div>
                </div>
                <div class="mb-3">
                  <label class="form-label">Email:</label>
                  <input type="text" class="form-control" name="user[email]" required>
                  <div class="form-text">Enter the email.</div>
                </div>
                <div class="mb-3">
                  <label class="form-label">Password:</label>
                  <input type="password" class="form-control" name="user[password]" required>
                  <div class="form-text">Enter a strong password.</div>
                </div>
                <div class="mb-3">
                  <label class="form-label">Status:</label>
                  <select class="form-select" name="user[status]" required>
                    <option selected="" disabled="" value="">Select</option>
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                  </select>
                </div>
                <br>
                <p class="msg-size">Picture size: 180px X 180px</p>
                <div class="Panel-galeria-int">
                  <div class="Pictures-des img-destacada">
                    <div class="Pictures-des-btn">
                      <button type="button" class="btn btn-outline-primary d-block W-full Buscar-imagen"><i class="bi bi-search"></i> Click for search the image</button>
                    </div>
                  </div>
                  <input type="file" id="destacada" style="display: none">
                </div>
                <br>
              </div>
            </div>
        </div>
        <div class="modal-footer justify-content-start">
          <button type="submit" class="btn btn-primary d-block">
            <div class="spinner-border spinner-border-sm load-form" role="status" style="display: none;">
              <span class="visually-hidden">Loading...</span>
            </div>
            Save User
          </button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <script src="assets/js/jquery-3.7.1.min.js"></script>
  <script src="assets/js/bootstrap.bundle.min.js"></script>
  <script src="assets/js/croppie.min.js"></script>
  <script src="assets/js/jquery.modal.min.js"></script>
  <script src="assets/js/script_users.js"></script>
</body>

</html>
