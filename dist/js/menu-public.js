$(document).ready(function () {
  $('#Drop').on('click', function () {
    console.log('Click');
    $('.Menu-completo').toggleClass('Menu-completo-oculto')

  });

  $('.Menu-togg li').on('click', function () {
    $('.Menu-completo').toggleClass('Menu-completo-oculto')
  });

  site_ln = localStorage.getItem('moviweb-ln');
  url = window.location.pathname.replaceAll("/", "");

  // console.log(url)

  if (!site_ln) {
    site_ln = 'en';
    localStorage.setItem("moviweb-ln", site_ln);
  } else {
    if (site_ln == 'es') {
      // $('.Ln-es').addClass('Ln-selected').show();
      // $('.Ln-en').hide().insertAfter('.Ln-es');
      if (url != 'es' && url.length == 0) {
        window.location.href = '/es';
      }
    } else {
      // $('.Ln-en').addClass('Ln-selected').show();
      // $('.Ln-es').hide().insertAfter('.Ln-en');
    }
  }

  $('.Ln-en').on('click', function () {
    // if (!$('.Ln-es').is(':visible')) {
    //   $('.Ln-es').css('display', 'table');
    // } else {
    //   $('.Ln-es').css('display', 'none');
    //   $(this).css('display', 'inline-block');
    //   $(this).insertBefore('.Ln-es');
    // }

    if (!$(this).hasClass('Ln-selected')) {
      localStorage.setItem("moviweb-ln", 'en');
      window.location.href = '/';
    }
  });

  $('.Ln-es').on('click', function () {
    // if (!$('.Ln-en').is(':visible')) {
    //   $('.Ln-en').css('display', 'table');
    // } else {
    //   $('.Ln-en').css('display', 'none');
    //   $(this).css('display', 'inline-block');
    //   $(this).insertBefore('.Ln-en');
    // }

    if (!$(this).hasClass('Ln-selected')) {
      localStorage.setItem("moviweb-ln", 'es');
      window.location.href = '/es';
    }
  });

});
