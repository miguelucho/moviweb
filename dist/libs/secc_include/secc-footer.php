<div class="Conten-foot">
  <div class="Conten-foot-int">
    <ul>
      <li><a href="https://www.instagram.com/movicoms/?hl=en" aria-label="Instagram of Movi communications" target="_blank"><i class="icon-instagram"></i></a></li>
      <li><a href="https://www.linkedin.com/company/movicoms" aria-label="Linkedin of Movi communications" target="_blank"><i class="icon-linkedin"></i></a></li>
      <li><a href="https://twitter.com/movi_coms?lang=en" aria-label="Twitter of Movi communications" target="_blank"><i class="icon-twitter"></i></a></li>
      <li><a href="https://www.facebook.com/movicoms/" aria-label="Facebook of Movi communications" target="_blank"><i class="icon-facebook"></i></a></li>
    </ul>
    <p>Copyright © Movi Communications 2021 All rights reserved, Designed by <a href="https://www.inngeniate.com/" target="_blank" style="font-family: CenturyGothic-Bold !important;">Inngeniate.com</a></p>
    <!-- <p>Copyright © Movi Communications 2021 All rights reserved, View Our <a href="#!">Privacy Policy</a>, Designed by <a href="https://www.inngeniate.com/" target="_blank">Inngeniate.com</a></p> -->
  </div>
</div>