<div class="Top">
  <div class="Top-int">
    <div class="Top-int-izq">
      <div class="Top-logo">
        <a href="/"> <img src="dist/assets/images/logo.svg" alt="" id="logocambio"></a>
      </div>
    </div>
    <div class="Top-int-der">
      <div class="Menu-drop Menu-oculto">
        <a href="javascript:void(0)" id="Drop">
          <img src="dist/assets/images/menu.svg" alt="">
        </a>
      </div>
      <div class="Top-menu Menu-completo Menu-completo-oculto">
        <nav>
          <ul class="Menu-togg">
            <li class="Menuhover"><span class="hover hover-1"><a data-scroll href="/es/#about">NOSOTROS</a></span></li>
            <li class="Menuhover"><span class="hover hover-1"><a data-scroll href="/es/#services">SERVICIOS</a></span></li>
            <li class="Menuhover"><span class="hover hover-1"><a data-scroll href="/es/#clients">CLIENTES</a></span></li>
            <li class="Menuhover"><span class="hover hover-1"><a data-scroll href="/es/#work">NUESTRO TRABAJO</a></span></li>
            <li class="Menuhover"><span class="hover hover-1"><a data-scroll href="#contact">CONTACTO</a></span></li>
            <li class="Menuhover Cambio-idioma"><span class="Ln-en"><a data-scroll href="#!">EN</a></span></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</div>