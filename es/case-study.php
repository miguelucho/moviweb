<?php
include('../dist/libs/conexion.php');

if (isset($_REQUEST['case'])) {
  $cases = $db
    ->where('url_cs', $_REQUEST['case'])
    ->objectBuilder()->get('casos_estudio_es');

  if ($db->count > 0) {
    $titulo = $cases[0]->titulo_cs;
    $seccion_1 = $cases[0]->seccion_1_cs;
    $seccion_2 = $cases[0]->seccion_2_cs;
    $texto_lateral = $cases[0]->texto_lateral_cs;
  } else {
    header('Location: ../index');
  }
} else {
  header('Location: ../index');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Movi Communications | Caso de Estudio | <?php echo $titulo ?></title>
  <?php include("../dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" href="dist/css/swiper-bundle.min.css" />
  <style>
    .Top-int-izq {
      width: 40%;
    }

    .Top-int-der {
      width: 60%;
    }

    .swiper-button-next,
    .swiper-button-prev {
      position: relative;
      display: inline-block;
      margin-right: 40px;
      height: 10px;
      outline: none;
    }

    .swiper-button-next:after,
    .swiper-button-prev:after {
      content: '';
    }

    .swiper-pagination {
      width: 77%;
      height: 4px;
      left: 0;
      top: 0;
      position: relative;
      display: inline-block;
    }

    .slider-container {
      width: 100%;
    }

    .swiper-container {
      height: 320px;
    }

    @media screen and (max-width: 1024px) {
      .Top-int-der {
        width: 100%;
      }

      .swiper-container {
        height: 380px !important;
      }
    }
  </style>
</head>

<body>

  <header>
    <?php include("../dist/libs/secc_include/secc-header-es.php") ?>
  </header>

  <section>
    <div class="Conten-superior Header-case">
      <div class="Conten-superior-sombra Header-case">
        <div class="Conten-superior-btn">
          <div class="Conten-titulo-case">
            <h2 class="Titulh2-case"><?php echo $titulo ?></h2>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section>
    <?php echo $texto_lateral ?>
  </section>

  <section>
    <?php echo $seccion_1 ?>
  </section>

  <section>
    <?php echo $seccion_2 ?>
  </section>

  <section>
    <?php include("../dist/libs/secc_include/secc-othercases-es.php") ?>
  </section>


  <?php include("../dist/libs/secc_include/secc-contacto-es.php") ?>

  <footer>
    <?php include("../dist/libs/secc_include/secc-footer.php") ?>
  </footer>
</body>
<script src="dist/js/jquery.min.js"></script>
<script src="dist/js/menu-scroll.js"></script>
<script src="dist/js/menu-public.js?<?php echo time()  ?>"></script>
<script src="dist/js/smooth-scroll.polyfills.js"></script>
<script src="dist/js/funcion-scroll.js"></script>
<script src="dist/js/swiper-bundle.min.js"></script>
<script src="dist/js/aos.js"></script>
<script>
  const swiper = new Swiper('.swiper-container', {
    slidesPerView: 2,
    spaceBetween: 20,
    slidesPerGroup: 1,
    // centeredSlides: true,
    centerInsufficientSlides: true,
    loop: false,
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20,
      },
      480: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      640: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      1024: {
        slidesPerView: 2,
        spaceBetween: 20
      }
    },

    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

    pagination: {
      el: ".swiper-pagination",
      type: "progressbar",
    }
  });

  AOS.init();
  $(function() {
    $('#slides').superslides({
      inherit_width_from: '.wide-container',
      inherit_height_from: '.wide-container',
      play: 3000,
      animation: 'fade'
    });
  });
</script>

</html>